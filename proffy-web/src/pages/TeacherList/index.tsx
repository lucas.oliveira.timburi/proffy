import React from "react";

import proffyLogoImg from "../../assets/images/logo.svg";
import whatsapp from "../../assets/images/icons/whatsapp.svg";
import giveClassesIcon from "../../assets/images/icons/give-classes.svg";
import purpleHearthIcon from "../../assets/images/icons/purple-heart.svg";

import "./styles.css";
import { Link } from "react-router-dom";
import Header from "../../compoents/Header";
import TeacherItem from "../../compoents/TeacherItem";

function TeacherList() {
  return (
    <div id="page-teacher-list" className="container">
      <Header title="Esses são os proffys disponíveis">
        <form id="search-teacher">
          <div className="input-block">
            <label htmlFor="subject">Matéria</label>
            <input type="text" id="subject" />
          </div>
          <div className="input-block">
            <label htmlFor="weekDay">Dia da semana</label>
            <input type="text" id="weekDay" />
          </div>
          <div className="input-block">
            <label htmlFor="time">Hora</label>
            <input type="text" id="time" />
          </div>
        </form>
      </Header>

      <main>
        <TeacherItem />
        <TeacherItem />
        <TeacherItem />
      </main>
    </div>
  );
}

export default TeacherList;
