import React from "react";

import proffyLogoImg from "../../assets/images/logo.svg";
import landingImg from "../../assets/images/landing.svg";
import studyIcon from "../../assets/images/icons/study.svg";
import giveClassesIcon from "../../assets/images/icons/give-classes.svg";
import purpleHearthIcon from "../../assets/images/icons/purple-heart.svg";

import "./styles.css";
import Header from "../../compoents/Header";

function TeacherForm() {
  return (
    <div id="page-teacher-form" className="container">
      <Header title="Que incrivel que você quer dar aulas" />
    </div>
  );
}

export default TeacherForm;
