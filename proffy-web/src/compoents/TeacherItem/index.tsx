import React from "react";

import proffyLogoImg from "../../assets/images/logo.svg";
import back from "../../assets/images/icons/back.svg";

import "./styles.css";
import { Link } from "react-router-dom";
import whatsapp from "../../assets/images/icons/whatsapp.svg";

interface HeaderProps {
}

const TeacherItem: React.FC<HeaderProps> = () => {
  return (
    <article className="teacher-item">
      <header>
        <img
          src="https://lh3.googleusercontent.com/ogw/ADGmqu-F4epl_7loAE_ujOvK8glKJiqdWKNbXhTL2oz_=s64-c-mo"
          alt="Lucas Oliveira"
        />
        <div>
          <strong>Lucas Oliveira</strong>
          <span>ReactNative</span>
        </div>
      </header>
      <p>
        Instrutor de programacao que ta por ai pretendendo se tornar um grande
        youtuber do momento.
      </p>
      <footer>
        <p>
          Preço/hora
          <strong>R$ 120,00</strong>
        </p>
        <button type="button">
          <img src={whatsapp} alt="Whatsapp" />
          Entrar em contato
        </button>
      </footer>
    </article>
  );
};

export default TeacherItem;
