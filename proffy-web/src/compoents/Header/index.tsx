import React from "react";

import proffyLogoImg from "../../assets/images/logo.svg";
import back from "../../assets/images/icons/back.svg";

import "./styles.css";
import { Link } from "react-router-dom";

interface HeaderProps {
  title: string;
}

const Header: React.FC<HeaderProps> = ({ title, ...props }) => {
  return (
    <header className="page-header">
      <div className="top-bar-container">
        <Link to="/">
          <img src={back} alt="Voltar" />
        </Link>
        <img src={proffyLogoImg} alt="Voltar" />
      </div>
      <div className="header-content">
        <strong>{title}</strong>
        {props.children}
      </div>
    </header>
  );
};

export default Header;
